## externalfeeds.py | by Javier Padilla | jpadilla@checkpoint.com
## This class will fetch reputation for diferent services

import json
import requests

class ExternalFeeds():
	def __init__(self, hashes):
		self.list_of_hashes = hashes
		
	def metadefender(self):
		api_endpoint = 'https://api.metadefender.com/v4/hash'
		headers = {'apikey': "e1caf727f1360cb35e21e267e3e67448", "includescandetails": "1" }
		body = {"hash": self.list_of_hashes }
		
		response = requests.post(api_endpoint, data=json.dumps(body), headers=headers)

		return response
		