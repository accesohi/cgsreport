###  This script will make a graphic of the type of attachments
from collections import namedtuple
import pandas
import matplotlib.pyplot as plt
import json
from externalfeeds import ExternalFeeds

class Report():
    def __init__(self, config ):
        ##- Read All .csv files
        ##- Office365
        self.csv_login_success = pandas.read_csv('csvdata/login_exitoso.csv')
        self.csv_login_failed = pandas.read_csv('csvdata/login_fallido.csv')
        self.csv_all_phishing = pandas.read_csv('csvdata/all_phishing.csv')
        self.csv_malware_sandbox = pandas.read_csv('csvdata/malware_by_sandbox.csv')
        ##- Gsuite
        self.csv_malware_sandbox = pandas.read_csv('csvdata/malware_by_sandbox.csv')
        self.cvs_gsuite_phishing = pandas.read_csv('csvdata/gsuite_phishing.csv')
        ##- The checkpoint email doamin should be removed from the dataset since
        ##- the beginning to avoid removing it on each analysis
        ##- e.g.   <domain>@<domain>-mail.checkpointcloudsec.com'
        ## -- Clean code
        ##- Read Config file
        #config_file = open('config.json')
        ##print(config)
        self.config = config
        ##print(self.config)
        #json.load(config_file)
        
    def percentage(self, part, whole):
        return 100 * float(part)/float(whole)
    
    
    def login_analysis(self, logintype = None ):
        '''login_analysis will return a Pie Chart as a drawing object
        logintype: True = succsefull login, False = failed login
        country: Is  a list if Excluded Countries, if the list is more than
                two countries, I thik we so
        '''
        excluded_country = []
        excluded_country = [self.config['customer_country']]
        if logintype == 1:
            #- Load Succesfull login data
            csv_data = self.csv_login_success['Country']
        else:
            csv_data = self.csv_login_failed['Country']
        
        allcountries= []
        countries = []
        count = []
        
        for country in csv_data:
            allcountries.append(country)
        ##- List of Unique countries
        setcountry = set(allcountries)

        for country in setcountry:
            if country in excluded_country:
                pass
            else:
                countries.append(country)
                count.append(allcountries.count(country ))
        return countries, count
    
    def login_analysis_o365(self):
        "Returns a named tuple with the login information"
        excluded_country = []
        excluded_country = [self.config['customer_country']]
        
        ## Failed Logins by country
        grouped = self.csv_login_failed.groupby(['Country'])
        total_countries_failed_login = len(grouped)
        grouped = grouped.size().reset_index(name='count')
        grouped = grouped.sort_values(by=['count'], ascending=False)
        #grouped = grouped.set_index( [ 'sender'] )
        #grouped = grouped.drop(['acescosa@acescosa-mail.checkpointcloudsec.com'])
        grouped = grouped.iloc[0:15]
        #grouped = grouped.reset_index()
        grouped = grouped.set_index( [ pandas.Index(range(1, 16))],'index' )
        top_failed_login_by_country = pandas.DataFrame(grouped).rename_axis('index')
        
        
        ## Failed Logins by city
        grouped = self.csv_login_failed.groupby(['Country', 'City'])
        total_city_failed_login = len(grouped)
        grouped = grouped.size().reset_index(name='count')
        grouped = grouped.sort_values(by=['count'], ascending=False)
        #grouped = grouped.set_index( [ 'sender'] )
        #grouped = grouped.drop(['acescosa@acescosa-mail.checkpointcloudsec.com'])
        grouped = grouped.iloc[0:15]
        #grouped = grouped.reset_index()
        grouped = grouped.set_index( [ pandas.Index(range(1, 16))],'index' )
        
        top_failed_login_by_city = pandas.DataFrame(grouped).rename_axis('index')
        total_failed_login = len(self.csv_login_failed)
        
        
        ## Sucessfull Logins by country
        grouped = self.csv_login_success.groupby(['Country'])
        total_countries_successfull_login = len(grouped)
        grouped = grouped.size().reset_index(name='count')
        grouped = grouped.sort_values(by=['count'], ascending=False)
        #grouped = grouped.set_index( [ 'sender'] )
        #grouped = grouped.drop(['acescosa@acescosa-mail.checkpointcloudsec.com'])
        grouped = grouped.iloc[0:15]
        
        # if len(grouped) < 15:
        #     toadd = 15 - len(grouped)
        #     for idx in toadd
        #         grouped.append({idx:[]})
        #grouped = grouped.reset_index()
        grouped = grouped.set_index( [ pandas.Index(range(1, len(grouped)+1))],'index' )
        top_successfull_login_by_country = pandas.DataFrame(grouped).rename_axis('index')
        
        
        ## Succesfull Logins by city
        grouped = self.csv_login_success.groupby(['Country', 'City'])
        total_city_successfull_login = len(grouped)
        grouped = grouped.size().reset_index(name='count')
        grouped = grouped.sort_values(by=['count'], ascending=False)
        #grouped = grouped.set_index( [ 'sender'] )
        #grouped = grouped.drop(['acescosa@acescosa-mail.checkpointcloudsec.com'])
        grouped = grouped.iloc[0:15]
        #grouped = grouped.reset_index()
        grouped = grouped.set_index( [ pandas.Index(range(1, len(grouped)+1))],'index' )
        
        top_successfull_login_by_city = pandas.DataFrame(grouped).rename_axis('index')
        total_succesfull_login = len(self.csv_login_success)
        
        
        
        
        
        Results = namedtuple('Results',['top_failed_login_by_country','top_failed_login_by_city',
                                        'total_failed_login','total_succesfull_login',
                                        'total_countries_failed_login', 'total_city_failed_login',
                                        'total_countries_successfull_login', 'total_city_successfull_login',
                                        'top_successfull_login_by_country', 'top_successfull_login_by_city' ])
        
        results = Results(top_failed_login_by_country,top_failed_login_by_city,
                          total_failed_login, total_succesfull_login,
                          total_countries_failed_login, total_city_failed_login,
                          total_countries_successfull_login, total_city_successfull_login,
                          top_successfull_login_by_country, top_successfull_login_by_city)
        return results
        
        
        
    def malware_analysis_o365(self):
        '''What malware was detected in Email and OneDrive'''
        
        
        
        #self.csv_malware_sandbox
        total_of_malware_in_email = len(self.csv_malware_sandbox)
       # total_of_malware_in_email = len(self.csv_malware_sandbox)
     
        
        grouped = self.csv_malware_sandbox.groupby('Name')
        grouped = grouped.size().reset_index(name='count')
        grouped = grouped.sort_values(by=['count','Name'], ascending=False)
        grouped = grouped.set_index( [ pandas.Index(range(1, len(grouped)+1))],'count' )
        top = pandas.DataFrame(grouped).rename_axis('index')
        top_malware_in_email = top
        
        grouped = self.csv_malware_sandbox.groupby('MIME type')
        total_of_attachment_mimetype = len(grouped)
        grouped = grouped.size().reset_index(name='count')
        grouped = grouped.sort_values(by=['count','MIME type'], ascending=False)
        grouped = grouped.set_index( [ pandas.Index(range(1, len(grouped)+1))],'count' )
        top = pandas.DataFrame(grouped).rename_axis('index')
        top_malware_file_type_in_email = top
        
        grouped = self.csv_malware_sandbox.groupby('Email Subject')
        grouped = grouped.size().reset_index(name='count')
        grouped = grouped.sort_values(by=['count','Email Subject'], ascending=False)
        grouped = grouped.set_index( [ pandas.Index(range(1, len(grouped)+1))],'count' )
        top = pandas.DataFrame(grouped).rename_axis('index')
        top_subject_with_malware_attch = top
        
       
        
        
        grouped = self.csv_malware_sandbox.groupby('Email Owner')
        top_users_that_receives_malware_in_email = grouped.size()
        
        
        total_of_malware_in_1drv= 1
        top_malware_in_1drv=1
        
        
        ### Look for malware hashes on diferents feeds for reputation
        ## First step is get a list of hashes
        ## Top 20 Hashes of malware
        ## Optional to be done if enabled at config file
        table = [("# Top", "Hash","Filename","Metadefender","Virus Total")]
        top = ()
        if self.config['features']['enable_metadefender_reputation_slide'] == "yes" :
            table_top = 20
            grouped = self.csv_malware_sandbox.groupby(['av_file_hash_md5','Name'])
            grouped = grouped.size().reset_index(name='count')
            grouped = grouped.sort_values(by=['count','av_file_hash_md5','Name'], ascending=False)
            grouped = grouped.iloc[0:21]
            grouped = grouped.set_index( [ pandas.Index(range(1, len(grouped)+1))],'count' )
            top = pandas.DataFrame(grouped).rename_axis('index')
            top_malware_hashes = top
            ### Top 20 malware reputation table
            ## a list of tuples
            ## Get_list
            
            hashes_list = grouped['av_file_hash_md5'].tolist()
            #   print(hashes_list)
            reputation = ExternalFeeds(hashes = hashes_list)
            response = reputation.metadefender()
            metadefender = json.loads(response.text)
            #for md in metadefender['data']:
            #    print(md['hash'])
                
            counter = 0
            
            for item, malwrname in zip(metadefender['data'], grouped['Name']) :
                counter += 1
                verdict = "%d AVs" % item['total_detected_avs']    
                table.append((counter,item['hash'], malwrname, verdict, 'Virus Total'))
            
            if counter < table_top:
                for x in range(0,table_top-counter):
                    table.append(['','',''])
        top_malware_hashes = top
        top_malware_reputation_table = table    
        #print(top_malware_reputation_table)
        
        
        Results = namedtuple('Results',['total_of_malware_in_email',
                                        'total_of_attachment_mimetype',
                                         'total_of_malware_in_1drv',
                                         'top_malware_in_email',
                                         'top_malware_file_type_in_email',
                                         'top_malware_hashes',
                                         'top_malware_reputation_table',
                                         'top_users_that_receives_malware_in_email',
                                         'top_malware_in_1drv',
                                         'top_subject_with_malware_attch'])
        
        
        ##- Returns a named tuple with the following items:
        ##-
        ##- total_of_malware_in_email: This is the total of items when custom query looks for malious verdict
        ##-     at TE -OR- infected verdict for AV scanning. Size of the dataframe  len(csv_malware)
        ##- xxxx
        ##- top_malware_in_email: This i a grouped dataframe by the name of the attachments.
        ##- top_users_that_receives_malware_in_email: Dataframe grouped by the Email Owner
        results = Results(total_of_malware_in_email,
                        total_of_attachment_mimetype,
                        total_of_malware_in_1drv,
                        top_malware_in_email,
                        top_malware_file_type_in_email,
                        top_malware_hashes,
                        top_malware_reputation_table,
                        top_users_that_receives_malware_in_email,
                        top_malware_in_1drv,
                        top_subject_with_malware_attch)
        return results
    
    def phishing_analysis_o365(self):
        '''SCL Analysis:
        -1: whitelist  0-1: No SPAM  5-6: SPAM  7,8,9: High Conf SPAM
        '''

        alldates = self.csv_all_phishing['Date']
        allscl = self.csv_all_phishing['O365 SCL']

        #- Determines Periosd of events
        dates = []
        for date in alldates:
            dates.append(date)
        dates.sort()
        
        ##- Count the  SCL Indexes
        scl_whitelist = 0
        scl_whitelist_values = [-1]
        
        scl_no_spam = 0
        scl_no_spam_values = [0,1]
        
        scl_spam = 0
        scl_spam_values = [5]
        
        scl_spam_high = 0
        scl_spam_high_values = [6]
        
        for scl in allscl:
            if scl in scl_whitelist_values:
                scl_whitelist += 1
            elif scl in scl_no_spam_values:
                scl_no_spam += 1
            elif scl in scl_spam_values:
                scl_spam += 1
            elif scl in scl_spam_high_values:
                scl_spam_high += 1
                
        ##- SCL Dataframe for making a graph
        grouped_byscl = self.csv_all_phishing.groupby('O365 SCL')
        grouped_byscl = grouped_byscl.size().reset_index(name='count')
        scl_dataframe = grouped_byscl
                
                
        ##- Most Phised Users will return  a dataframe

        #grouped = self.csv_all_phishing.groupby(['Found in mailbox','O365 SCL'])
        newdf = pandas.DataFrame({'email':self.csv_all_phishing['Found in mailbox'],
                     'scl':self.csv_all_phishing['O365 SCL'],
                     'sender':self.csv_all_phishing['Sender'],
                     'subject':self.csv_all_phishing['Subject']})
        #newdf.sort_values(by='email')
        
        grouped_by_email = newdf.groupby(['email'])   
        grouped_by_email = grouped_by_email.size().reset_index(name='count')
        grouped_by_email = grouped_by_email.sort_values(by=['count','email'], ascending=False)
        grouped_by_email = grouped_by_email.set_index( [ 'email'] )
        grouped_by_email = grouped_by_email.drop([self.config['cpemail']])
        grouped_by_email = grouped_by_email.iloc[0:20]
        grouped_by_email = grouped_by_email.reset_index()
        most_phished_users = grouped_by_email.set_index( [ pandas.Index(range(1, 21))],'email' )
        most_phished_users = pandas.DataFrame(most_phished_users).rename_axis('index')
        ###### - End of most phished users
        
        
        

        ## - Emails Phishing Sources
        phishersdf = pandas.DataFrame({'email':self.csv_all_phishing['Found in mailbox'],
                     'scl':self.csv_all_phishing['O365 SCL'],
                     'sender':self.csv_all_phishing['Sender'],
                     'subject':self.csv_all_phishing['Subject']})
        #newdf.sort_values(by='email')
        
        grouped = newdf.groupby(['sender'])   
        grouped = grouped.size().reset_index(name='count')
        grouped = grouped.sort_values(by=['count','sender'], ascending=False)
        #grouped = grouped.set_index( [ 'sender'] )
        #grouped = grouped.drop(['acescosa@acescosa-mail.checkpointcloudsec.com'])
        grouped = grouped.iloc[0:20]
        #grouped = grouped.reset_index()
        top_phishers = grouped.set_index( [ pandas.Index(range(1, 21))],'sender' )
        top_phishers = pandas.DataFrame(top_phishers).rename_axis('index')



        ## - End of Phishing  Sources


        ##- Top 20 Phishing Subjects
        
        phishersdf = pandas.DataFrame({'email':self.csv_all_phishing['Found in mailbox'],
                     'scl':self.csv_all_phishing['O365 SCL'],
                     'sender':self.csv_all_phishing['Sender'],
                     'subject':self.csv_all_phishing['Subject']})
        #newdf.sort_values(by='email')
        
        grouped = newdf.groupby(['subject'])   
        grouped = grouped.size().reset_index(name='count')
        grouped = grouped.sort_values(by=['count','subject'], ascending=False)
        #grouped = grouped.set_index( [ 'sender'] )
        #grouped = grouped.drop(['acescosa@acescosa-mail.checkpointcloudsec.com'])
        grouped = grouped.iloc[0:20]
        #grouped = grouped.reset_index()
        top_subject = grouped.set_index( [ pandas.Index(range(1, 21))],'subject' )
        top_subject = pandas.DataFrame(top_subject).rename_axis('index')


        
        ##- End of Top 20 Phishing Subject



        total_missed_eop = scl_whitelist+scl_no_spam
        total_of_phishing = len(allscl)

        Results = namedtuple('Results',['scl_whitelist', 'scl_no_spam', 'scl_spam', 'scl_spam_high', 'scl_dataframe',
                                        'total_of_phishing', 'total_missed_eop','percent_missed_eop',
                                        'poc_date_begin','poc_date_ended','most_phished_users',
                                        'top_phishers','top_subject'])

        
        results = Results(scl_whitelist, scl_no_spam, scl_spam, scl_spam_high, scl_dataframe,
                          total_of_phishing, total_missed_eop, self.percentage( total_missed_eop, total_of_phishing ),
                          dates[0][0:10], dates[-1][0:10], most_phished_users,
                          top_phishers, top_subject)
        #print(results)
        return results
    
    
    
        def gsuite_phishing_analisys(self):
            
            Results = namedtuple('Results',['total_of_phishing', 'total_missed_eop','percent_missed_eop',
                                        'poc_date_begin','poc_date_ended','most_phished_users',
                                        'top_phishers','top_subject'])

        
            total_of_phishing = len(self.csv_phishing_gsuite)
            total_missed_eop=0
            percent_missed_eop=0
            poc_date_begin = 0
            poc_date_ended = 0
            most_phished_users = 0
            top_phishers=0
            top_subject=0
        
        
            results = Results( total_of_phishing, total_missed_eop, self.percentage( total_missed_eop, total_of_phishing ),
                          dates[0][0:10], dates[-1][0:10], most_phished_users,
                          top_phishers, top_subject)
            
            