import os
import time
##- Data Processing 
import pandas
import numpy as np
import matplotlib.pyplot as plt
import messages as msg
from six.moves import urllib
import requests
import json
##- Own libreries
from reportdata import Report
from CGSReport import messages as msgngn
##- Report creation
from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus import (BaseDocTemplate, SimpleDocTemplate, Paragraph, NextPageTemplate,
                                Image, Spacer, PageTemplate, Frame, PageBreak, Table, TableStyle)
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.pagesizes import landscape, letter, A4
from reportlab.lib.units import inch
from reportlab.lib import colors
from reportlab.graphics.shapes import Drawing
from reportlab.graphics.charts.piecharts import Pie
from reportlab.graphics.charts.barcharts import VerticalBarChart
import styles
import tablestyles


##- Load configuration files config.json
with open('config.json') as config_file:
    config = json.load(config_file)

lang = config['language']

filename = "CGSReport-"+config['customer']+".pdf"
tmplpath = "CGSReport/template/" + config['doc_template'] + '/'

##- Get Analisis of csvfiles
report  = Report( config = config)
## phishing_analysis is a namedtuple
phishing_analysis = report.phishing_analysis_o365()
malwareo365 = report.malware_analysis_o365()

##- Start Messagin engine, this is all the labels and printed messages
##- are shown in different languages
msgs = msgngn.Messages( config = config)
systemMsgs = msgs.system()
##- Create labes for all the document
coversMsgs = msgs.covers()
phishingMsgs = msgs.phishing(data = phishing_analysis)





##- DOCUMENT CREATION

##- This is the PDF container
canvas = Canvas(filename)

##- Document Step #1:  Create Canvas


def make_cover(canvas,cgsreport):
    cpcover = tmplpath + "front_cover.png"
    #cpcover = "images/cpcover.png"
    customer_logo = "images/customer_logo.png"
    canvas.setPageSize(landscape((1920,1080)))
    canvas.drawImage(cpcover, 0, 0, width=1920, height=1080, mask='auto')
    canvas.drawImage(customer_logo, 1300, 400, width=564, height=670, mask='auto')
    
def make_back_cover(canvas,cgsreport):
    ##- GET QRCode
    try:
        headers = { "X-API-KEY": "F5AuigKjSfYRNMdxdTsS3axuRkJv8sS3b03P3uP1" }
        apiurl = "https://2077hazaie.execute-api.us-east-1.amazonaws.com/ver1/QRCodeGenerator/"
        encodingRequest = {"OriginalText": config['qrcode_message'] }
        apiResponse = requests.post( apiurl, headers = headers, data = json.dumps(encodingRequest))
        qrcode_image_url = json.loads(apiResponse.text)
        qrcode_image_url = qrcode_image_url["ImageURL"]
        qrcodeimg = requests.get(qrcode_image_url)
        open("images/back_qrcode.png", "wb").write(qrcodeimg.content)
    except:
        print(systemMsgs.connection_failed)
    cpcover = tmplpath + "back_cover.png"
    qrcode = "images/back_qrcode.png"
    canvas.setPageSize(landscape((1920,1080)))
    canvas.drawImage(cpcover, 0, 0, width=1920, height=1080, mask='auto')
    canvas.drawImage(qrcode, 700, 300, width=500, height=500, mask='auto')
    
def make_transition(canvas,cgsreport):
    cpcover = "images/cptransition.png"
    canvas.setPageSize(landscape((1920,1080)))
    canvas.drawImage(cpcover, 0, 0, width=1920, height=1080, mask='auto')
    
def make_content(canvas,cgsreport):
    background = "images/cpcontent.png"
    canvas.setPageSize(landscape((1920,1080)))
    canvas.drawImage(background, 0, 0, width=1920, height=1080, mask='auto')
                       
def make_pie_chart(dataframe = None, figsize = None, label = None, filename = None):
    pie = dataframe.plot.pie(y='count', figsize=figsize, legend=None)
    pie = pie.set_title( label = label )    
    plt.savefig('images/' + filename + '.png')
    
def make_scl_bar_chart(dataframe = None, figsize = None, label = None, filename = None ):
    ax = dataframe.plot.bar(x='O365 SCL' ,y='count', rot = 0, width=0.9, use_index=False, figsize=(5, 5))
    categoryNames = ['-1 INBOX','1 INBOX','5 Junk','6 Junk', '9 Junk']
    xpos = [i for i, _ in enumerate(categoryNames)]
    #ax = ax.legend(legends)
    ax = ax.set_title( label = label )
    plt.xticks(xpos, categoryNames)

    plt.savefig('images/' + filename + '.png')
    
def make_table_data(top = None, dataframe = None, table_header = None, groupedby = None):
    '''make_table_data will create a list of lists (it can also be touples)
        top: is the number of the elements to be shown on the table
        dataframe: is the data to be analyzed
        tableheader: list or tuple withe the titles on the table
    '''
    data = []
    data.append(table_header)

    mycount = 0
    for index in range(0,top):
        #print(index)
        #print(mycount)
        #if mycount >= mostphished_top:
        if mycount >= len(dataframe):
            break
        #print(mycount)
        data.append(('# ' + str(mycount+1),
                     dataframe[groupedby][mycount+1],
                     dataframe['count'][mycount+1]))
        mycount += 1

    if mycount < top:
        for x in range(0,top-mycount):
            data.append(('','',''))
    return data

##- Define Flowables
sample_style_sheet  = getSampleStyleSheet()

author_name = Paragraph( config['author_name'] + ' | ' + config['author_role'] , styles.author_style)
author_email = Paragraph(config['author_email'], styles.author_style)


cover_title = Paragraph('Check Point CloudGuard SaaS',styles.cover_title)
cover_subtitle = Paragraph(coversMsgs.cover_subtitle, styles.cover_subtitle)

##- Page #1 - Presentation Cover
##- Data of the person that creates the report, period covered, customer logo



##- Append Flowables

flow = []
cover_title_flow = []

##- Slide #0 Cover - This is just the cover of the document with the title of the document
##- The author data and the customer logo that is placed directly at the canvas
cover_title_flow.append(cover_title)
cover_title_flow.append(Spacer(1,65))
cover_title_flow.append(cover_subtitle)
cover_title_flow.append(Spacer(1,60))
cover_title_flow.append(Paragraph(time.ctime(),styles.author_style))
cover_title_flow.append(Spacer(1,70))

cover_author_flow = []
cover_author_flow.append(author_name)
cover_author_flow.append(Spacer(1,55))
cover_author_flow.append(author_email)

# ## - uncomment for agenda
# ##- Slide #1 Agenda
# ##- Setup Page
# agenda_title_flow = []
# agenda_title_flow.append(NextPageTemplate('content_template_with_title'))
# agenda_title_flow.append(PageBreak())
# ##- Agenda Content
# agenda_title_flow.append(Paragraph( msg.d022[lang] , styles.content_title))
# #agenda_title_flow.append(Paragraph( msg.agenda[lang] , styles.content_title))



##--End of agenda--------------------------------------------------------------



##-  Phishing Section
##- Slide #2
##- Transition slide for Account Take Over
flow.append(NextPageTemplate('transition_template'))
flow.append(PageBreak())
flow.append(Paragraph( msg.phishing_transition[lang] , styles.cover_title))

##- Phishing detected Vs EOP and ATP
flow.append(NextPageTemplate('content_template_two_columns'))
flow.append(PageBreak())
##- Title
flow.append(Paragraph(msg.scl_title[lang] , styles.content_title))

##- Generate Graphic for SCL
##- This is the graphic provided by RnD but done with pandas

make_scl_bar_chart(dataframe = phishing_analysis.scl_dataframe, figsize = None, label = phishingMsgs.emails_byscl_barchar_title, filename = 'scl_barchart' )
scl_bar_chart = Image('images/scl_barchart.png', 650, 500)

flow.append(Spacer(1,80))
flow.append(Spacer(1,30))
flow.append(scl_bar_chart)

sclchart = Image('images/scl.png', 650, 500)

flow.append(Spacer(1,80))
flow.append(sclchart)
flow.append(Paragraph( phishingMsgs.o365_scl_analysis, styles.content_footer))

## - Most Phished Users
flow.append(NextPageTemplate('content_template_two_columns_large'))
flow.append(PageBreak())
##- Title
flow.append(Paragraph(msg.mostphished_title[lang] , styles.content_title))

## - Most Phished Users Table
data = []
data.append(['Top # ','Target email','# Phishing'])
#- Top 25 Users
mostphished_top = 20

mycount = 0
for index in range(0,mostphished_top):
    #print(mycount)
    if mycount >= mostphished_top:
        break
    #print(mycount)
    data.append(['# ' + str(mycount+1),
                 phishing_analysis.most_phished_users['email'][mycount+1],
                 phishing_analysis.most_phished_users['count'][mycount+1]])
    mycount += 1

if mycount < mostphished_top:
    for x in range(0,mostphished_top-mycount):
        data.append(['','',''])
    


total_rows = 20
mostphished_table=Table(data, colWidths=[inch*2, inch*5, inch*2] ,rowHeights=inch/2 )
mostphished_table.setStyle( TableStyle (tablestyles.table_style(total_rows)) )

flow.append(Spacer(1,80))
flow.append(mostphished_table)


### Bar graphic for most Phished users

mostphished_reversed = phishing_analysis.most_phished_users.iloc[::-1]
legends = []
for num in range(1,21):
    legends.append(num)
#print(legends)
#print(mostphished_reversed)
ax = mostphished_reversed.plot.barh(x='email' ,y='count', rot = 0, width=0.9, use_index=False, figsize=(5, 5))
#ax = ax.legend(legends)
ax = ax.set_title( label = msg.mostphished_label[lang] )

plt.savefig('images/most_phished_users.png')

#- Add image
most_phished_bar = Image('images/most_phished_users.png', 900, 700)

flow.append(most_phished_bar)

## end of most Phised Users





## - Top 20 phishier's emails

flow.append(NextPageTemplate('content_template_two_columns_large'))
flow.append(PageBreak())
##- Title
flow.append(Paragraph(msg.phishers_title[lang] , styles.content_title))

## - Top Phishers emails from attackers Table
data = []
data.append(['Top # ',"Phisher's address",'# Phishing'])
#- Top 25 Users
mostphished_top = 20

mycount = 0
for index in range(0,mostphished_top):
    #print(mycount)
    if mycount >= mostphished_top:
        break
    #print(mycount)
    data.append(['# ' + str(mycount+1),
                 phishing_analysis.top_phishers['sender'][mycount+1],
                 phishing_analysis.top_phishers['count'][mycount+1]])
    mycount += 1

if mycount < mostphished_top:
    for x in range(0,mostphished_top-mycount):
        data.append(['','',''])
    


total_rows = 20
mostphished_table=Table(data, colWidths=[inch*2, inch*5, inch*2] ,rowHeights=inch/2)
mostphished_table.setStyle( TableStyle (tablestyles.table_style(total_rows)) )

flow.append(Spacer(1,80))
flow.append(mostphished_table)


### Bar graphic for most Phished users

mostphished_reversed = phishing_analysis.top_phishers.iloc[::-1]
legends = []
for num in range(1,21):
    legends.append(num)
#print(legends)
#print(mostphished_reversed)
ax = mostphished_reversed.plot.barh(x='sender' ,y='count', rot = 0, width=0.9, use_index=False, figsize=(5, 5))
#ax = ax.legend(legends)
ax = ax.set_title( label = msg.mostphished_label[lang] )

plt.savefig('images/top_phisher_senders.png')

#- Add image
most_phished_bar = Image('images/top_phisher_senders.png', 900, 700)

flow.append(most_phished_bar)

## End of --  Top 20 phishier's emails



## - Top 20 phisher's subject
flow.append(NextPageTemplate('content_template_with_title'))
flow.append(PageBreak())
##- Title
flow.append(Paragraph(msg.phishing_subject_title[lang] , styles.content_title))

## - Top Phishers emails from attackers Table
data = []
data.append(['Top # ',"Email subject",'# Phishing'])
#- Top 25 Users
mostphished_top = 20

mycount = 0
for index in range(0,mostphished_top):
    #print(mycount)
    if mycount >= mostphished_top:
        break
    #print(mycount)
    data.append(['# ' + str(mycount+1),
                 phishing_analysis.top_subject['subject'][mycount+1],
                 phishing_analysis.top_subject['count'][mycount+1]])
    mycount += 1

if mycount < mostphished_top:
    for x in range(0,mostphished_top-mycount):
        data.append(['','',''])
    

total_rows = 20
mostphished_table=Table(data, colWidths=[inch*2, inch*15, inch*2] , rowHeights=inch/2)
mostphished_table.setStyle(TableStyle( [('BACKGROUND',(0,0),(2,0),colors.HexColor('#99004d')),
                                    ('TEXTCOLOR',(0,0),(2,0),colors.HexColor('#ffffff')),
                                    ('FONTSIZE',(0,0),(2,0),22),
                                    ('VALIGN',(0,0),(2,0),'TOP'),
                                    ('ALIGN',(0,0),(2,0),'CENTER'),
                                    ### Items
                                    ('BACKGROUND',(0,1),(2,total_rows),colors.HexColor('#ffccdd')),
                                    ('TEXTCOLOR',(0,1),(2,total_rows),colors.HexColor('#000000')),
                                    ('FONTSIZE',(0,1),(2,total_rows),18),
                                    ('VALIGN',(0,1),(2,total_rows),'MIDDLE'),
                                    ('ALIGN',(0,1),(0,total_rows),'CENTER'),
                                    ('ALIGN',(2,1),(2,total_rows),'CENTER'),
                                    ]))

flow.append(Spacer(1,80))
flow.append(mostphished_table)

###---  End Of Phishing Section --- End of Phishing Section -----








##- MALWARE SECTION
##- This section talks about:
##  + Malware found by SandBox
##  + Formats used on malware deliver
##  + SaaS with more malware

##- Slide #2
##- Transition slide for Malware
flow.append(NextPageTemplate('transition_template'))
flow.append(PageBreak())
flow.append(Paragraph( msg.malware_transition[lang] , styles.cover_title))


##- About slide for malware


flow.append(NextPageTemplate('content_template_with_title'))
flow.append(PageBreak())
flow.append(Paragraph( msg.malware_about_title[lang] , styles.content_title))
flow.append(Spacer(1,100))
flow.append(Paragraph( msg.malware_about_text[lang] , styles.general_text))
flow.append(Spacer(1,20))
flow.append(Paragraph( msg.malware_about_text_b[lang] , styles.general_text))







## - Top malware in Email
flow.append(NextPageTemplate('content_template_two_columns_large'))
flow.append(PageBreak())

##- Title
flow.append(Paragraph(msg.top_malicious_files_in_email_title[lang] , styles.content_title))


malware_top = 10

#- Pie Chart image
imagefile = 'top_malware_in_email'
make_pie_chart(dataframe = malwareo365.top_malware_in_email, figsize = (4, 4),
                           label = 'Malicious files in e-mail',
                           filename = imagefile)

top_malware_in_email_pie = Image('images/' + imagefile + '.png', 400, 400)
flow.append(top_malware_in_email_pie)

##- Malware by name table

data = make_table_data(top = malware_top, dataframe = malwareo365.top_malware_in_email,
                       table_header = ('Top # ','Filename','# Events'), groupedby = 'Name')

malwr_by_name_table=Table(data, colWidths=[inch*2, inch*5,inch*2] ,rowHeights=inch/2 )
malwr_by_name_table.setStyle(TableStyle (tablestyles.table_style(total_rows = malware_top)) )

#flow.append(Spacer(1,10))
flow.append(malwr_by_name_table)

#- Pie Chart image
imagefile = 'top_malware_file_type_in_email'
make_pie_chart(dataframe = malwareo365.top_malware_file_type_in_email, figsize = (4, 4),
                           label = 'MIME type of Malicious files',
                           filename = imagefile)

top_malware_file_type_in_email_pie = Image('images/' + imagefile + '.png', 400, 400)
flow.append(top_malware_file_type_in_email_pie)


##- Malware by mime type
malware_table_data = make_table_data(top = malware_top, dataframe = malwareo365.top_malware_file_type_in_email,
                       table_header = ('Top # ','MIME type','# Events'), groupedby = 'MIME type')

malware_table=Table(malware_table_data, colWidths=[inch*2, inch*5,inch*2] ,rowHeights=inch/2 )
malware_table.setStyle(TableStyle (tablestyles.table_style(total_rows = malware_top)) )

flow.append(malware_table)


## end of top malware in email


## - Top 20 Malicious email's subjects

flow.append(NextPageTemplate('content_template_with_title'))
flow.append(PageBreak())
##- Title
flow.append(Paragraph(msg.top_malicious_subjects_title[lang] , styles.content_title))

## - Top Phishers emails from attackers Table
data = []
data.append(['Top # ',"Email Subject",'# Events'])
#- Top 25 Users
table_top = 20

mycount = 0
for index in range(0,table_top):
    #print(mycount)
    if mycount >= table_top:
        break
    #print(mycount)
    data.append(['# ' + str(mycount+1),
                 malwareo365.top_subject_with_malware_attch['Email Subject'][mycount+1],
                 malwareo365.top_subject_with_malware_attch['count'][mycount+1]])
    mycount += 1

if mycount < table_top:
    for x in range(0,table_top-mycount):
        data.append(['','',''])
    

total_rows = 20
malwaresubject_table=Table(data, colWidths=[inch*2, inch*15, inch*2] , rowHeights=inch/2)
malwaresubject_table.setStyle(TableStyle( tablestyles.table_style(total_rows = table_top) ))

flow.append(Spacer(1,80))
flow.append(malwaresubject_table)

##- End of top subjects in email with malicious attachment

    
if config['features']['enable_metadefender_reputation_slide'] == "yes" :
    print("#- Adding Metadefender reputation table")
    ## - Top 20 malware reputation feeds

    flow.append(NextPageTemplate('content_template_with_title'))
    flow.append(PageBreak())
    ##- Title
    flow.append(Paragraph(msg.top_malicious_reputation[lang] , styles.content_title))
    
    data = malwareo365.top_malware_reputation_table
        
    
    total_rows = 20
    malware_table=Table(data, colWidths=[inch*2, inch*5, inch*6,inch*4,inch*4] , rowHeights=inch/2)
    malware_table.setStyle(TableStyle( tablestyles.table_style_reputation(total_rows = total_rows) ))
    
    flow.append(Spacer(1,80))
    flow.append(malware_table)

##- End of top sources in email with malicious attachment


### - END OF MALWAWRE SECTION


##- Transition slide for Account Take Over
flow.append(NextPageTemplate('transition_template'))
flow.append(PageBreak())
flow.append(Paragraph( msg.f456[lang] , styles.cover_title))


##-- Analysis de inicios de sesion


##- Primero se muestran los inicios fallidos y luego los exitosos

failed_logins = report.login_analysis_o365()

#print(failed_logins)

##- Slide #3   FAILED BY COUNTRY
##- Inicios de Sesion Fallidos Fuera del Lugar de Origen DE la Empresa
##- u otras localidades donde la empresa tiene filiales
##- Set-up Page
flow.append(NextPageTemplate('content_template_two_columns'))
flow.append(PageBreak())
##- Title
flow.append(Paragraph(msg.failed_logins_by_country_title[lang] , styles.content_title))

##- Pie Chart Matplotlib

pie = failed_logins.top_failed_login_by_country.plot.pie(y='count', figsize=(5, 5), legend=None)
#ax = ax.legend(legends)
pie = pie.set_title( label = 'Failed logins by country' )

plt.savefig('images/top_failed_by_country_pie.png')

#- Add image
failed_by_country = Image('images/top_failed_by_country_pie.png', 600, 600)

flow.append(failed_by_country)

##- End of matplot

##- Pie Chart - ReportLab
country_top = 15

failed_by_country_table_data = make_table_data(top = country_top, dataframe = failed_logins.top_failed_login_by_country,
                       table_header = ('Top # ','Country','# Log-in'), groupedby = 'Country')

failed_by_country_table=Table(failed_by_country_table_data, colWidths=[inch*2, inch*5,inch*2] ,rowHeights=inch/2 )
failed_by_country_table.setStyle(TableStyle (tablestyles.table_style(total_rows = country_top)) )

flow.append(Spacer(1,10))
flow.append(failed_by_country_table)

flow.append(Paragraph(msg.failed_logins_footer_by_country[lang] , styles.content_footer))






##- Slide #3   FAILED BY City
##- Inicios de Sesion Fallidos Fuera del Lugar de Origen DE la Empresa
##- u otras localidades donde la empresa tiene filiales
##- Set-up Page
flow.append(NextPageTemplate('content_template_two_columns'))
flow.append(PageBreak())
##- Title
flow.append(Paragraph(msg.failed_logins_by_city_title[lang] , styles.content_title))

##- Pie Chart Matplotlib

pie = failed_logins.top_failed_login_by_city.plot.pie(y='count', figsize=(5, 5), legend=None)
#ax = ax.legend(legends)
pie = pie.set_title( label = 'Failed logins by city' )

plt.savefig('images/top_failed_by_city_pie.png')

#- Add image
failed_by_city = Image('images/top_failed_by_city_pie.png', 600, 600)

flow.append(failed_by_city)

##- End of matplot

##- Pie Chart - ReportLab
top = 15
total_rows = 15


data = []
data.append(['# Top','Country / City','# Log-in'])
#- Top 15 Countries
total_logins = len(failed_logins.top_failed_login_by_country)
#print(total_logins)
mycount = 0
for index in range(0,total_logins):
    #print(mycount)
    if mycount >= top:
        break
    data.append(['# ' + str(mycount+1),
                 failed_logins.top_failed_login_by_city['Country'][mycount+1] + '/' +failed_logins.top_failed_login_by_city['City'][mycount+1],
                 failed_logins.top_failed_login_by_city['count'][mycount+1]])
    mycount += 1


countries_table=Table(data, colWidths=[inch*2, inch*5, inch*2] ,rowHeights=inch/2 )
countries_table.setStyle(TableStyle( [('BACKGROUND',(0,0),(2,0),colors.HexColor('#99004d')),
                                        ('TEXTCOLOR',(0,0),(2,0),colors.HexColor('#ffffff')),
                                        ('FONTSIZE',(0,0),(2,0),25),
                                        ('VALIGN',(0,0),(2,0),'TOP'),
                                        ('ALIGN',(0,0),(2,0),'CENTER'),
                                        ### Items
                                        ('BACKGROUND',(0,1),(2,total_rows),colors.HexColor('#ffccdd')),
                                        ('TEXTCOLOR',(0,1),(2,total_rows),colors.HexColor('#000000')),
                                        ('FONTSIZE',(0,1),(2,total_rows),20),
                                        ('VALIGN',(0,1),(2,total_rows),'MIDDLE'),
                                        ('ALIGN',(0,1),(0,total_rows),'CENTER'),
                                        ('ALIGN',(2,1),(2,total_rows),'CENTER'),
                                        ]))


flow.append(Spacer(1,10))
flow.append(countries_table)
flow.append(Paragraph(msg.failed_logins_footer_by_city[lang] , styles.content_footer))
## - End of FAILED Logins by City





## - End of FAILED Logins



##- SUCCESFULL LOGINS


##- Primero se muestran los inicios fallidos y luego los exitosos

failed_logins = report.login_analysis_o365()

##- Slide #3   Succesfull BY COUNTRY
##- Inicios de Sesion Fallidos Fuera del Lugar de Origen DE la Empresa
##- u otras localidades donde la empresa tiene filiales
##- Set-up Page
flow.append(NextPageTemplate('content_template_two_columns'))
flow.append(PageBreak())
##- Title
flow.append(Paragraph(msg.successfull_logins_by_country_title[lang] , styles.content_title))

##- Pie Chart Matplotlib

pie = failed_logins.top_successfull_login_by_country.plot.pie(y='count', figsize=(5, 5), legend=None)
#ax = ax.legend(legends)
pie = pie.set_title( label = 'Successfull logins by country' )

plt.savefig('images/top_successfull_by_country_pie.png')

#- Add image
succesfull_by_country = Image('images/top_successfull_by_country_pie.png', 600, 600)

flow.append(succesfull_by_country)

##- End of matplot

##- Pie Chart - ReportLab
top = 15
total_rows = 15


#countries, count = report.login_analysis(logintype=0)

#if len(countries) > top:
#    countries = countries[0:top]
#    count = count[0:top]

data = []
data.append(['# Top','Country','# Log-in'])
#- Top 15 Countries
total_logins = len(failed_logins.top_successfull_login_by_country)
#print(total_logins)
mycount = 0
for index in range(0,total_logins):
    #print(mycount)
    if mycount >= top:
        break
    data.append(['# ' + str(mycount+1),
                 failed_logins.top_successfull_login_by_country['Country'][mycount+1],
                 failed_logins.top_successfull_login_by_country['count'][mycount+1]])
    mycount += 1

if mycount < top:
    for x in range(0,top-mycount):
        data.append(['',''])
        
countries_table=Table(data, colWidths=[inch*2, inch*5, inch*2] ,rowHeights=inch/2 )
countries_table.setStyle(TableStyle( [('BACKGROUND',(0,0),(2,0),colors.HexColor('#99004d')),
                                        ('TEXTCOLOR',(0,0),(2,0),colors.HexColor('#ffffff')),
                                        ('FONTSIZE',(0,0),(2,0),25),
                                        ('VALIGN',(0,0),(2,0),'TOP'),
                                        ('ALIGN',(0,0),(2,0),'CENTER'),
                                        ### Items
                                        ('BACKGROUND',(0,1),(2,total_rows),colors.HexColor('#ffccdd')),
                                        ('TEXTCOLOR',(0,1),(2,total_rows),colors.HexColor('#000000')),
                                        ('FONTSIZE',(0,1),(2,total_rows),20),
                                        ('VALIGN',(0,1),(2,total_rows),'MIDDLE'),
                                        ('ALIGN',(0,1),(0,total_rows),'CENTER'),
                                        ('ALIGN',(2,1),(2,total_rows),'CENTER'),
                                        ]))


flow.append(Spacer(1,10))
flow.append(countries_table)
flow.append(Paragraph(msg.succesfull_logins_footer[lang] , styles.content_footer))






##- Slide #3   SUCESSFULL BY City
##- Inicios de Sesion Fallidos Fuera del Lugar de Origen DE la Empresa
##- u otras localidades donde la empresa tiene filiales
##- Set-up Page
flow.append(NextPageTemplate('content_template_two_columns'))
flow.append(PageBreak())
##- Title
flow.append(Paragraph(msg.successfull_logins_by_city_title[lang] , styles.content_title))

##- Pie Chart Matplotlib

pie = failed_logins.top_successfull_login_by_city.plot.pie(y='count', figsize=(5, 5), legend=None)
#ax = ax.legend(legends)
pie = pie.set_title( label = 'Successfull logins by city' )

plt.savefig('images/top_successfull_by_city_pie.png')

#- Add image
succesfull_by_city = Image('images/top_successfull_by_city_pie.png', 600, 600)

flow.append(succesfull_by_city)

##- End of matplot

##- Pie Chart - ReportLab
top = 15
total_rows = 15


#countries, count = report.login_analysis(logintype=0)

#if len(countries) > top:
#    countries = countries[0:top]
#    count = count[0:top]

data = []
data.append(['# Top','Country / City','# Log-in'])
#- Top 15 Countries
#total_logins = len(failed_logins.top_successfull_login_by_city)
#print(total_logins)
mycount = 0
for index in range(0,total_logins):
    #print(mycount)
    if mycount >= top:
        break
    data.append(['# ' + str(mycount+1),
                 failed_logins.top_successfull_login_by_city['Country'][mycount+1] + '/' +failed_logins.top_successfull_login_by_city['City'][mycount+1],
                 failed_logins.top_successfull_login_by_city['count'][mycount+1]])
    mycount += 1

if mycount < top:
    for x in range(0,top-mycount):
        data.append(['',''])
        
countries_table=Table(data, colWidths=[inch*2, inch*5, inch*2] ,rowHeights=inch/2)
countries_table.setStyle(TableStyle( [('BACKGROUND',(0,0),(2,0),colors.HexColor('#99004d')),
                                        ('TEXTCOLOR',(0,0),(2,0),colors.HexColor('#ffffff')),
                                        ('FONTSIZE',(0,0),(2,0),25),
                                        ('VALIGN',(0,0),(2,0),'TOP'),
                                        ('ALIGN',(0,0),(2,0),'CENTER'),
                                        ### Items
                                        ('BACKGROUND',(0,1),(2,total_rows),colors.HexColor('#ffccdd')),
                                        ('TEXTCOLOR',(0,1),(2,total_rows),colors.HexColor('#000000')),
                                        ('FONTSIZE',(0,1),(2,total_rows),20),
                                        ('VALIGN',(0,1),(2,total_rows),'MIDDLE'),
                                        ('ALIGN',(0,1),(0,total_rows),'CENTER'),
                                        ('ALIGN',(2,1),(2,total_rows),'CENTER'),
                                        ]))


flow.append(Spacer(1,10))
flow.append(countries_table)
flow.append(Paragraph(msg.succesfull_logins_by_city_footer[lang] , styles.content_footer))
## - End of SUCESSFULL Logins by City


## - Last Cover with a QR Code
flow.append(NextPageTemplate('back_cover'))
flow.append(PageBreak())
flow.append(Spacer(1,50))
flow.append(Paragraph(msg.generation_banner[lang],styles.back_cover_banner))


## - End of Successfull Logins




allFlows = cover_title_flow + cover_author_flow + flow #agenda_title_flow + flow
##- Frames

all_page_frame = Frame(0.0 * inch, 0.0 * inch, 1920, 1080,
                    leftPadding=0, rightPadding=0,
                    topPadding=0, bottomPadding=0,
                    id='all_page_frame')


cover_author_frame = Frame(100, 100, 800, 200,
                    leftPadding=0, rightPadding=0,
                    topPadding=0, bottomPadding=0,
                    id='cover_author_frame', showBoundary=0)
cover_author_frame.addFromList(cover_author_flow, canvas)

cover_title_frame = Frame(100, 500, 1000, 200,
                    leftPadding=0, rightPadding=0,
                    topPadding=0, bottomPadding=0,
                    id='cover_title_frame', showBoundary=0)
cover_title_frame.addFromList(cover_title_flow, canvas)




##- All content slides have a tilte on the top
content_title_frame = Frame(50, 950, 1500, 100,
                    leftPadding=0, rightPadding=0,
                    topPadding=0, bottomPadding=0,
                    id='content_title_frame', showBoundary=0)

content_body_frame = Frame(50, 100, 1700, 800,
                    leftPadding=0, rightPadding=0,
                    topPadding=0, bottomPadding=0,
                    id='content_body_frame', showBoundary=0)

##- Frames column
content_column_left = Frame( 50, 300, 900, 600,
                    leftPadding=0, rightPadding=0,
                    topPadding=0, bottomPadding=0,
                    id='content_column_left', showBoundary=0)

content_column_left_large = Frame( 50, 100, 900, 800,
                    leftPadding=0, rightPadding=0,
                    topPadding=0, bottomPadding=0,
                    id='content_column_left', showBoundary=0)
#content_column_left.addFromList(cover_title_flow, canvas)


content_column_right = Frame(950, 300, 900, 600,
                    leftPadding=0, rightPadding=0,
                    topPadding=0, bottomPadding=0,
                    id='content_column_right', showBoundary=0)

content_column_right_large = Frame(950, 100, 900, 800,
                    leftPadding=0, rightPadding=0,
                    topPadding=0, bottomPadding=0,
                    id='content_column_right', showBoundary=0)
#content_column_right.addFromList(cover_title_flow, canvas)

content_footer = Frame(50, 80, 1820, 200,
                    leftPadding=0, rightPadding=0,
                    topPadding=0, bottomPadding=0,
                    id='content_column_right', showBoundary=0)
#content_footer.addFromList(cover_title_flow, canvas)


back_cover_tiny_banner = Frame(400, 20, 1500, 100,
                    leftPadding=0, rightPadding=0,
                    topPadding=0, bottomPadding=0,
                    id='back_cover_tiny_banner', showBoundary=0)


## - Page Templates - this are the templates for each sheet.


cover_template = PageTemplate(id='cover_landscape',frames=[cover_title_frame, cover_author_frame],
                              onPage=make_cover)

back_cover = PageTemplate(id='back_cover',frames=[back_cover_tiny_banner], onPage=make_back_cover)

transition_template = PageTemplate(id='transition_template',frames=[cover_title_frame],
                                   onPage=make_transition)

content_template_with_title = PageTemplate(id='content_template_with_title',
                                frames=[content_title_frame, content_body_frame],
                                onPage=make_content)

##- Page Template for Succesfull and Failed, Logins.
##- This page has title and two columns

content_template_two_columns_large = PageTemplate(id='content_template_two_columns_large',
                                frames=[content_title_frame, content_column_left_large,
                                        content_column_right_large],
                                onPage=make_content)

content_template_two_columns = PageTemplate(id='content_template_two_columns',
                                frames=[content_title_frame, content_column_left,
                                        content_column_right, content_footer],
                                onPage=make_content)

##-----------------------------------------------------------------------------


##- Build the report
cgsreport = BaseDocTemplate(filename,
                            allowSplitting=1,
                            showBoundary=0,
                            #encrypt=config['password']
                            )
cgsreport.addPageTemplates([cover_template, back_cover,  transition_template, content_template_with_title,
                            content_template_two_columns, content_template_two_columns_large])
cgsreport.build(allFlows)
#os.system('cgsreport.pdf')



print(systemMsgs.goodbye)









