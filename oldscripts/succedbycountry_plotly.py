###  This script will make a graphic of the type of attachments
import pandas
import plotly.graph_objects as go


df = pandas.read_csv('csvdata/login_exitoso.csv')



mimedata = df['Country']

allmime = []
types = []
count = []
colors = ["#204912","#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#8c564b", "#2fbcb2", "#d1c147", "#d14747"]

for mimetype in mimedata:
    allmime.append(mimetype)
    
setmime = set(allmime)


for x in setmime:
    if x in ['Colombia']:
        pass
    else:
        types.append(x)
        count.append(allmime.count(x))
    
print types, count

fig = go.Figure(data=[go.Pie(labels=types, values=count)])
fig.show()

